package com.onespatial.networkrail.eai.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class TableDTO {

    /**
     *
     */
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @SerializedName("tableName")
    @Expose
    private String tableName;

    @SerializedName("columnDTOList")
    @Expose
    private List<ColumnDTO> columnDTOList;

    public TableDTO() {
    }

    public String getTableName() {
        return this.tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public List<ColumnDTO> getColumnDTOList() {
        return this.columnDTOList;
    }

    public void setColumnDTOList(List<ColumnDTO> columnDTOList) {
        this.columnDTOList = columnDTOList;
    }

    @Override
    public String toString() {
        return "TableDTO{" +
                "tableName='" + tableName + '\'' +
                ", columnDTOList=" + columnDTOList +
                '}';
    }
}
