package com.onespatial.networkrail.eai.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ColumnDTO {

    /**
     *
     */
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @SerializedName("columnName")
    @Expose
    private String columnName;

    @SerializedName("columnTypeName")
    @Expose
    private String columnTypeName;

    @SerializedName("columnDisplaySize")
    @Expose
    private int columnDisplaySize;

    @SerializedName("precision")
    @Expose
    private int precision;

    @SerializedName("scale")
    @Expose
    private int scale;

    @SerializedName("mandatory")
    @Expose
    private boolean mandatory = true;

    @SerializedName("displayLabel")
    @Expose
    private String displayLabel;

    @SerializedName("grouping")
    @Expose
    private int grouping;

    public ColumnDTO() {
    }

    public String getColumnName() {
        return this.columnName;
    }

    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    public String getColumnTypeName() {
        return this.columnTypeName;
    }

    public void setColumnTypeName(String columnTypeName) {
        this.columnTypeName = columnTypeName;
    }

    public int getColumnDisplaySize() {
        return this.columnDisplaySize;
    }

    public void setColumnDisplaySize(int columnDisplaySize) {
        this.columnDisplaySize = columnDisplaySize;
    }

    public int getPrecision() {
        return this.precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }

    public int getScale() {
        return this.scale;
    }

    public void setScale(int scale) {
        this.scale = scale;
    }

    public boolean isMandatory() {
        return this.mandatory;
    }

    public void setMandatory(boolean mandatory) {
        this.mandatory = mandatory;
    }

    public String getDisplayLabel() {
        return this.displayLabel;
    }

    public void setDisplayLable(String displayLable) {
        this.displayLabel = displayLabel;
    }

    public int getGrouping() {
        return this.grouping;
    }

    public void setGrouping(int grouping) {
        this.grouping = grouping;
    }

    @Override
    public String toString() {
        return "ColumnDTO{" +
                "columnName='" + columnName + '\'' +
                ", columnTypeName='" + columnTypeName + '\'' +
                ", columnDisplaySize=" + columnDisplaySize +
                ", precision=" + precision +
                ", scale=" + scale +
                ", mandatory=" + mandatory +
                ", displayLabel='" + displayLabel + '\'' +
                ", grouping=" + grouping +
                '}';
    }
}
