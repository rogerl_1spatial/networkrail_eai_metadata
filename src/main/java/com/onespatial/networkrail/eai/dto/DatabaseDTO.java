package com.onespatial.networkrail.eai.dto;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

public class DatabaseDTO {

    /**
     *
     */
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @SerializedName("databaseName")
    @Expose
    private String databaseName;

    @SerializedName("tableDTOList")
    @Expose
    private List<TableDTO> tableDTOList;

    public DatabaseDTO() {

    }

    public String getDatabaseName() {
        return this.databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public List<TableDTO> getTableDTOList() {
        return this.tableDTOList;
    }

    public void setTableDTOList(List<TableDTO> tableDTOList) {
        this.tableDTOList = tableDTOList;
    }

    @Override
    public String toString() {
        return "DatabaseDTO{" +
                "databaseName='" + databaseName + '\'' +
                ", tableDTOList=" + tableDTOList +
                '}';
    }
}
