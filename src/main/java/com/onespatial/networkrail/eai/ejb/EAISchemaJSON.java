package com.onespatial.networkrail.eai.ejb;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.onespatial.networkrail.eai.dto.ColumnDTO;
import com.onespatial.networkrail.eai.dto.DatabaseDTO;
import com.onespatial.networkrail.eai.dto.TableDTO;
import org.hibernate.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import javax.ejb.LocalBean;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.ejb.TransactionManagement;
import javax.ejb.TransactionManagementType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@Singleton
@LocalBean
@Startup
@TransactionManagement(TransactionManagementType.CONTAINER)
public class EAISchemaJSON {

    private static final int ZERO = 0;
    private static final int ONE = 1;
    private static final int THREE = 3;
    /**
     *
     */
    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    /**
     *
     */
    @PersistenceContext(unitName = "EAIOraclePersistenceUnit")
    private EntityManager mySQLEntityManager;

    /**
     *
     */
    public EAISchemaJSON() {
    }

    @PostConstruct
    public void init() {
        logger.info(">>>>> EAISchemaJSON init .....");
        this.doWork();
    }

    /**
     *
     */
    public String doWork() {
        logger.info(">>>>> doWork .....");

        Session session = mySQLEntityManager.unwrap(Session.class);
        logger.info(">>>>> doWork session = {}", session);

        session.doWork(connection -> getMySQLByJDBCConnection(connection));
        //String json = session.doReturningWork(connection -> getMySQLByJDBCConnection(connection));

        //noinspection ReturnOfNull
        return null;
    }

    /**
     * @param connection Connection
     */
    private void getMySQLByJDBCConnection(Connection connection) throws SQLException {
        logger.info(">>>>> getMySQLByJDBCConnection connection isOpen = {}", !connection.isClosed());

        DatabaseMetaData databaseMetaData = connection.getMetaData();
        ResultSet resultSetDatabaseMetaData = databaseMetaData.getTables(null, null, "%", null);

        DatabaseDTO databaseDTO = new DatabaseDTO();

        Statement statement;
        ResultSet resultSet;

        List<TableDTO> tableDTOArrayList = new ArrayList<>(ZERO);

        while (resultSetDatabaseMetaData.next()) {
            logger.info(">>>>> databaseMetaData resultSet Database 1 = {}", resultSetDatabaseMetaData.getString(ONE));

            String databaseName = resultSetDatabaseMetaData.getString(ONE);
            databaseDTO.setDatabaseName(databaseName);

            String tableName = resultSetDatabaseMetaData.getString(THREE);
            logger.info(">>>>> databaseMetaData resultSet tableName = {}", tableName);

            TableDTO tableDTO = new TableDTO();
            tableDTO.setTableName(tableName);

            StringBuilder stringBuilder = new StringBuilder(ZERO);
            stringBuilder.append("SELECT * FROM ");
            stringBuilder.append(tableName);

            statement = connection.createStatement();
            resultSet = statement.executeQuery(stringBuilder.toString());

            ResultSetMetaData tableResultSetMetaData = resultSet.getMetaData();
            logger.info(">>>>> getMySQLByJDBCConnection tableResultSetMetaData getColumnCount = {}", tableResultSetMetaData.getColumnCount());

            List<ColumnDTO> columnDTOList = new ArrayList<>(ZERO);

            for (int columnNumber = 1; columnNumber <= tableResultSetMetaData.getColumnCount(); columnNumber++) {
                ColumnDTO columnDTO = new ColumnDTO();

                //logger.info(">>>>> resultSet getCatalogName = {}", tableResultSetMetaData.getCatalogName(columnNumber));
                logger.info(">>>>> resultSet getColumnName = {}", tableResultSetMetaData.getColumnName(columnNumber));
                //logger.info(">>>>> resultSet getColumnType = {}", tableResultSetMetaData.getColumnType(columnNumber));
                logger.info(">>>>> resultSet getColumnTypeName = {}", tableResultSetMetaData.getColumnTypeName(columnNumber));
                logger.info(">>>>> resultSet getColumnDisplaySize = {}", tableResultSetMetaData.getColumnDisplaySize(columnNumber));
                logger.info(">>>>> resultSet getPrecision = {}", tableResultSetMetaData.getPrecision(columnNumber));
                logger.info(">>>>> resultSet getScale = {}", tableResultSetMetaData.getScale(columnNumber));
                logger.info(">>>>> resultSet getColumnLabel = {}", tableResultSetMetaData.getColumnLabel(columnNumber));

                columnDTO.setColumnName(tableResultSetMetaData.getColumnName(columnNumber));
                columnDTO.setColumnTypeName(tableResultSetMetaData.getColumnTypeName(columnNumber));

                int columnDisplaySize = tableResultSetMetaData.getColumnDisplaySize(columnNumber);
                logger.info(">>>>> 2 columnDisplaySize = {}", columnDisplaySize);

                columnDTO.setColumnDisplaySize(columnDisplaySize);

                int precision = tableResultSetMetaData.getPrecision(columnNumber);
                logger.info(">>>>> 2 precision = {}", precision);

                columnDTO.setPrecision(precision);

                columnDTOList.add(columnDTO);
            }

            tableDTO.setColumnDTOList(columnDTOList);

            tableDTOArrayList.add(tableDTO);
        }

        databaseDTO.setTableDTOList(tableDTOArrayList);

        this.createJSON(databaseDTO);
    }

    /**
     * @param databaseDTO DatabaseDTO
     */
    private void createJSON(final DatabaseDTO databaseDTO) {
        Gson gson = EAISchemaJSON.getGsonBuilder().create();
        String json = gson.toJson(databaseDTO);

        logger.info(">>>>> databaseDTO = {}", json);
    }

    /**
     * @return GsonBuilder
     */
    private static GsonBuilder getGsonBuilder() {

        final String GSON_DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

        return new GsonBuilder()
                .excludeFieldsWithoutExposeAnnotation()
                .setDateFormat(GSON_DATE_FORMAT)
                .serializeNulls()
                .setPrettyPrinting();
    }
}
